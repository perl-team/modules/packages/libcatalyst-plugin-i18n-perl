libcatalyst-plugin-i18n-perl (0.10-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 14:17:07 +0000

libcatalyst-plugin-i18n-perl (0.10-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Add missing build dependency on libmodule-install-perl.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libmro-compat-perl,
      perl.
    + libcatalyst-plugin-i18n-perl: Drop versioned constraint on
      libmro-compat-perl, perl in Depends.
    + libcatalyst-plugin-i18n-perl: Drop versioned constraint on
      libcatalyst-modules-perl in Replaces.
    + libcatalyst-plugin-i18n-perl: Drop versioned constraint on
      libcatalyst-modules-perl in Breaks.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 22:05:19 +0100

libcatalyst-plugin-i18n-perl (0.10-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 15:38:32 +0100

libcatalyst-plugin-i18n-perl (0.10-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: fix long description.
    Thanks to Beatrice Torracca for the bug report and the proposed fix.
    (Closes: #770494)

  [ Damyan Ivanov ]
  * Mark package as autopkgtest-able
  * Declare conformance to Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Sun, 21 Jun 2015 15:36:40 +0000

libcatalyst-plugin-i18n-perl (0.10-2) unstable; urgency=medium

  * fix version of broken/replaces libcatalyst-modules-perl

 -- Damyan Ivanov <dmn@debian.org>  Thu, 15 May 2014 11:10:41 +0300

libcatalyst-plugin-i18n-perl (0.10-1) unstable; urgency=low

  * Initial Release. Spin-off from libcatalyst-modules-perl, where it was
    bundled.

 -- Damyan Ivanov <dmn@debian.org>  Wed, 14 May 2014 17:38:26 +0300
